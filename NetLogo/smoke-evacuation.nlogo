; Sistemi Complessi: Modelli e Simulazione
; Evacuazione di un Edificio in Caso di Incendio
; Davide Marcon (781344), Alessio Riccardo Villa (780748)

extensions [      ; include or allow:
  csv             ; csv managing
  matrix          ; use of matrix
]

globals[
  building        ; patch-set containing only the walkable patches (or tiles)
  cmaprix         ; cognitive map matrix
  hScore          ; distances from goal nodes
  openings        ; set of openings
  exits           ; set of exits
  exit-counter    ; count the number of agents that use the exits
  open-visibility ; list of average visibility for each opening
  open-pmove      ; list of average move probability for each opening
  croom           ; map of the building
  varya           ; list of variations (visibility or pmove)
  smoke-room      ; list of rooms, item r is true if in room r there is smoke
]

patches-own[
  isMap            ; true if patch is part of the map, false otherwise
  smoke-visibility ; visibility parameter taken from FDS
  blocks           ; number of block in the cell given a period of time
  locked           ; true if the patch is occupated by an agent or if a pedestrian reserved it
  npedo-per-time   ; number of pedestrians in the patch in 50 time-steps
  proom            ; room to which belong the patch
]

turtles-own[
  agent-alert-tick ; tick in which the agent will be alerted
  alerted?         ; true if the agent is alerted, false otherwise
  c-path           ; cognitive path the agent is following
  current-node     ; current cognitive map's node where the agent is
  goal-rnode       ; current patch destination
  goal-cnode       ; cmaprix's node of destination in respect to the agent
  candidate        ; next patch the agent moves to
  room             ; room in which the agent is
]

;; Setup the world
to setup
  clear-all
  ca
  reset-ticks
  setup-patch-map
  setup-probability 50
  setup-openings
  setup-cognitive-map
  setup-agents
end

;; Run the simulation
to go
  ; every graph has its own method
  ifelse plot-choice = 1 [
    update-patch-map
  ][
    ifelse plot-choice = 2 [
      update-patch-map-plots
    ][
      ifelse plot-choice = 3 [
        update-patch-map-plots-2
      ][
        update-patch-map
      ]
    ]
  ]
  ; the cognitive map is updated every 10 time steps
  if remainder ticks 10 = 0 and ticks != 0 [
    update-cognitive-map
  ]
  update-agents
  update-plot-exits
  ; the simulation stops when there are no more agents
  if not any? turtles [ stop ]
  if remainder ticks 50 = 0 and ticks != 0 and plot-choice = 2 [ stop ]
  tick
end

;; Run the simulation one tick at a time, no controls are made
to go-nocontrol
  update-patch-map-plots
  update-cognitive-map
  update-agents
  tick
end

;; Setup the real map
to setup-patch-map
  file-close-all
  ; retrieve the real map
  let fileList csv:from-file "new_map.csv"
  ; choose the smoke map
  ifelse select-map = "1-smoke" [
    file-open "devc-notime.csv"
  ][
    file-open "devc2-notime.csv"
  ]
  ; use the new_map.csv in order to establish the patches which are part of the walkable map
  foreach sort patches [ the-patch ->
    ask the-patch [
      carefully [ ; try
        set isMap item pxcor item pycor fileList
      ][] ; catch
    ]
  ]
  ; if a patch is walkable then is part of the agentset building
  set building patches with [isMap = 1]

  ask patches[
    ; set the visibility and the belonging room
    ifelse member? self building [
      set smoke-visibility 1
      set pcolor white
      set npedo-per-time n-values 50 [0] ; number of pedestrian in the previuos 50 time steps
      ; set the room in which the patch is
      ifelse pxcor < 38 and pxcor > 19 and pycor < 102 and pycor > 0 [
        set proom 0
      ] [
        ifelse pxcor < 79 and pxcor > 38 and pycor < 102 and pycor > 61 [
          set proom 1
        ][
          ifelse pxcor < 79 and pxcor > 38 and pycor < 61 and pycor > 19 [
            set proom 2
        ][
            ifelse pxcor < 79 and pxcor > 38 and pycor < 19 and pycor > 0 [
              set proom 3
            ][
              ifelse pxcor < 19 and pxcor > 0 and pycor < 68 and pycor > 35 [
                set proom 4
              ][ set proom -1 ]
            ]
          ]
        ]
      ]

    ] [set pcolor brown] ; the patches not walkable are brown
  ]
  set smoke-room n-values 5 [false] ; in the beginning there is no smoke in the rooms
end

;; Setup the list of openings, each opening is represented by a patch-set
to setup-openings
  let op0 patches with [pycor = 102 and pxcor > 26 and pxcor < 32]
  let op1 patches with [pxcor = 38  and pycor > 79 and pycor < 85]
  let op2 patches with [pycor = 61  and pxcor > 48 and pxcor < 54]
  let op3 patches with [pycor = 61  and pxcor > 63 and pxcor < 69]
  let op4 patches with [pxcor = 38  and pycor > 37 and pycor < 43]
  let op5 patches with [pycor = 19  and pxcor > 56 and pxcor < 62]
  let op6 patches with [pxcor = 38  and pycor > 6  and pycor < 12]
  let op7 patches with [pxcor = 19  and pycor > 48 and pycor < 54]
  let op8 patches with [pycor = 0   and pxcor > 26 and pxcor < 32]

  set openings (list op0 op1 op2 op3 op4 op5 op6 op7 op8)
  set exits (list 0 8) ; in this experiment the exits are opening 0 and 8
  set exit-counter (list 0 0) ; setup the counter that indicates the exits' usage

  ; the initial visibility and move probability of the cognitive nodes is 1 (maximum possible)
  set open-visibility n-values 9 [1]
  set open-pmove n-values 9 [1]

  ; color the openings' patch at the beginning
  foreach openings[ open ->
    ask open[set pcolor 18]
  ]

  set varya n-values 9 [0]
  set-open-visibility
  set-open-pmove

end

;; Setup the cognitive map, read csv containing cognitive map, h scores and room openings
to setup-cognitive-map
  set cmaprix readCSV "cmap.csv"
  set hscore readCSV "hscore-new.csv"
  set croom readCSV "room_opening_matrix.csv"
end

;; Setup agents in the world
to setup-agents
  ; create n agents and dislocate them
  create-turtles n-agents
  if agent-zero [
    set-agents-room-0 n-agents
  ]
  set-agents-room-1 n-agents
  set-agents-room-2 n-agents
  set-agents-room-4 n-agents

  ask turtles [
    ; set the tick in which the agent will be alerted
    ifelse select-map != "0-smoke"[
      set agent-alert-tick alert-tick + random-float 10
    ][
      set agent-alert-tick 0
    ]
    let gScore 5000000000 ; maximum g score
    let turt self
    ; foreach exit calculate A*, the less expensive route will be chosen
    foreach exits [ exit ->
      let A a-star turt exit
      if item 1 A < (gScore + 1) [
        ; if two routes have the same weight the choice is random
        ifelse (item 1 A) = gScore [
          let rnd (random 2)
          if rnd = 1 [set c-path item 0 A]
        ] [set c-path item 0 A]
        set gScore item 1 A
      ] ; if item 1 A = gScore
    ]
    set shape "turtle"
    set color gray ; the turtles are gray until they're alerted
    set alerted? false
    ifelse plot-choice = 1 [
      pen-down
    ][
      if plot-choice = 2 or plot-choice = 3 [
        hide-turtle
      ]
    ]
  ]
end

;; Setup the number of blocks for each walkable cell in the previous time steps window
to setup-probability [window]
  ask building [
    set blocks n-values window [0]
  ]
end

;; Update real map's values at every tick and show the smoke visibility
to update-patch-map
  if file-at-end? [ stop ]
  ; data contain the row corresponding to the visibility values of the current time step
  let data csv:from-row file-read-line
  let n 0
  ; foreach patch in the building update the visibility value
  foreach sort building [ tile ->
    ask tile [
      if select-map != "0-smoke"[
        set smoke-visibility normalitation item n data 0 30
      ]
      if plot-choice = 0 and select-map != "0-smoke" [
        set pcolor 9.9 * smoke-visibility ; update the visual map
      ]
    ]
    set n n + 1
  ]
end

;; Update real map's values at every tick and show the number of agents on patches given a time window
to update-patch-map-plots
  if file-at-end? [ stop ]
  let data csv:from-row file-read-line
  let n 0
  foreach sort building [ tile ->
    ask tile [
      if select-map != "0-smoke"[
        set smoke-visibility normalitation item n data 0 30
      ]
    ]
    set n n + 1
  ]
  ask building [
    ifelse any? turtles-on self [
      set npedo-per-time insert-item 49 npedo-per-time 1
      ] [
      set npedo-per-time insert-item 49 npedo-per-time 0
    ]
    set npedo-per-time remove-item 0 npedo-per-time
    let col 20 + 9.9 * ( sum npedo-per-time / 50 )
    let i-col 9.9 - col
    set pcolor i-col
  ]
end

;; Update real map's values at every tick and show the last tick in which the patch has been occupied
to update-patch-map-plots-2
  if file-at-end? [ stop ]
  let data csv:from-row file-read-line
  let n 0
  foreach sort building [ tile ->
    ask tile [
      if select-map != "0-smoke"[
        set smoke-visibility normalitation item n data 0 30
      ]
    ]
    set n n + 1
  ]
  let vplot matrix:make-constant world-width world-height 0
  let x 0
  let y world-height - 1
  foreach sort patches [ op ->
    ask op [
      if any? turtles-on self [
        matrix:set vplot x y ticks
        let col 120
        ; normalization change in relation to the experiment
        ifelse select-map = "0-smoke" [
          set col col + 9.9 * ( normalitation ticks 0 140 )
        ] [
          ifelse sigma-power < 3.5 [
            set col col + 9.9 * ( normalitation ticks 0 700 )
          ] [
            set col col + 9.9 * ( normalitation ticks 0 280 )
          ]
        ]
        set pcolor (9.9 - col)
      ]
    ]
    set x x + 1
    if x = world-width [
      set x 0
      set y y - 1
    ]
  ]

end

;; Update the cognitive map
to update-cognitive-map
  set-open-visibility
  set-open-pmove
end

;; Update agents' state and knowledge
to update-agents

  smoke-in-room?
  ; check if the agent has to be alerted
  ask turtles [ if ticks > agent-alert-tick [
    set alerted? true
    set color red
    ]
  ]

  ask turtles [
    let n 0
    let opa openings-available room
    foreach opa [ opening ->
      ; if there are significant changes in the environment then the agent recalculates the path
      if item opening varya = 1 [
        let gScore 5000000000
        let turt self
        foreach exits [ exit ->
          let A a-star turt exit
          ;show A
          if item 1 A < (gScore + 1) [
            ifelse (item 1 A) = gScore [
              let rnd (random 2)
              if rnd = 1 [set c-path item 0 A]
            ] [set c-path item 0 A]
            set gScore item 1 A
          ]
        ]
      ]
    ]
  ]

  ask turtles [
    let t-patch patch-here ; gets the patch on which the turtle agent is
    let id item 0 c-path
    let op item id openings
    set goal-rnode min-one-of op [distance t-patch] ; chooses the destination patch
    face goal-rnode
    ask patch-set (list neighbors patch-here) [
      ifelse any? turtles-on self
      [set locked 1]
      [set locked 0]
    ]
  ]

  ; move the agent in the environment
  let prob-move random-float 1
  ask turtles [
    ; the agent moves according to the smoke visibility
    if prob-move <= [smoke-visibility] of patch-here [
      let destination goal-rnode
      let t-patch patch-here
      let rm room
      ; find the nearest node reachable and not locked from the destination
      set candidate min-one-of (patch-set (list neighbors patch-here)) with
        [isMap = 1 and (locked = 0 or self = t-patch) and (proom = rm or proom < 0)] [distance destination] ; select the next patch to move to
      ask candidate [
        set locked 1 ; lock the patch, avoid conflicts
      ]
    ]
  ]

  ; actual movement of agents
  ask turtles [
    if alerted? and (prob-move <= [smoke-visibility] of patch-here) [
      face candidate
      move-to candidate
      ; a not moving agent is blocked
      ifelse candidate = patch-here [
        set blocks insert-item 24 blocks 1
      ] [
        set blocks insert-item 24 blocks 0
      ]
      set blocks remove-item 0 blocks
      if candidate = goal-rnode [
        let tmp item 0 c-path
        set c-path remove-item 0 c-path
        ; when the current goal is reached agents move on to the next
        ifelse empty? c-path [
          if member? patch-here (item 0 openings) [
            set exit-counter replace-item 0 exit-counter (item 0 exit-counter + 1)
          ]
          if member? patch-here (item 8 openings) [
            set exit-counter replace-item 1 exit-counter (item 1 exit-counter + 1)
          ]
          die
        ][
          set room item tmp matrix:get-row croom room
        ]
      ]
    ]
  ]
end

;; Check if there are smoke or alerted agents in a certain radius, if true the agent becomes alerted
to smoke-in-room?
  ask turtles [
    if ([smoke-visibility] of patch-here < 1 and proom > -1 and agent-alert-tick > ticks + 10) or alerted? [
      set smoke-room replace-item room smoke-room true
      set agent-alert-tick (ticks + 1 + random-float 5)
    ]
    let alert false
    ask turtles in-radius 7 [ ; if an alerted agent is nearby the pedestrian becomes alerted itself
      if alerted?[
        set alert true
      ]
    ]
    if alert and (agent-alert-tick > ticks + 6) [set agent-alert-tick (ticks + 1 + random-float 5)]
  ]
end

;; Retrieve the openings of the room in which the agent is
to-report openings-available [a-room]
  let opng matrix:get-row croom a-room
  let baz []
  let n 0
  foreach opng [ x ->
    if x > -1 [
      set baz lput n baz
    ]
    set n n + 1
  ]
  report baz
end

;; A* pathfinding algorithm, given an agent and a goal node finds the optimal path
to-report a-star [agent goal]

  ; closed and open set initialization
  let closed-set []
  let open-set openings-available [room] of agent
  ; came-from holds the minimum path
  let came-from n-values first matrix:dimensions cmaprix [-1]

  ; g and f score initialization (h score is already set)
  ; hScore[n] = estimated and optimitic cost from n node to goal
  ; gScore[n] = movement cost from start to n node
  ; fScore[n] = estimated cost of path passing through n node (gscore[n] + hscore[n])

  let chScore matrix:get-row hScore goal
  let fScore matrix:get-row hScore goal

  let gScore n-values first matrix:dimensions cmaprix [5000000]

  ; foreach possible start node compute the cost of reaching it from agent's actual position
  foreach open-set [ start ->
    set gScore replace-item start gScore (start-weight agent start)
  ]

  while [not (empty? open-set)] [
    let nearest nearest-node fScore open-set
    if nearest = goal [ ; if nearest is the destination then stop
      let path recontruct-path came-from goal
      let t-score item goal gScore + item goal chScore
      report (list path t-score)
    ]
    set open-set remove nearest open-set
    set closed-set lput nearest closed-set

    let neigh-list neighbours nearest
    foreach neigh-list [ y ->
      let t-isBetter false

      if not(member? y closed-set) [
        let t-score item nearest gScore + weight nearest y

        if not(member? y open-set) [
          set open-set lput y open-set
          set t-isBetter true
        ]
        if t-score < item y gScore [ set t-isBetter true ]

        if t-isBetter [
          set came-from replace-item y came-from nearest
          set gScore replace-item y gScore t-score
          let newF item y gScore + item y chScore
          set fScore replace-item y fScore newF
        ]
      ]
    ]
  ]
  report []

end

;;;;;;;;;; UTILITY ;;;;;;;;;;

;; Read csv and report the equivalent matrix
to-report readCSV[csv]
  report matrix:from-row-list csv:from-file csv
end

;; Report the node in openset having the lowest fScore[] value
to-report nearest-node [fScore open-set]
  let index -1
  let minF 5000000
  let n length open-set - 1
  while [n > -1] [
    let x item n open-set
    let y item x fScore
    if y < minF [
      set minF y
      set index x
    ]
    set n n - 1
  ]
  report index
end

;; Report the neighbours list of node
to-report neighbours [node]

  let neigh-list []
  let tmplist matrix:get-row cmaprix node
  let n 0
  foreach tmplist [ x ->
    if x > 0 [ set neigh-list lput n neigh-list ]
    set n n + 1
  ]
  report neigh-list
end

;; Function used to calculate the weight of visibility
to-report sigma [number]
  report number ^ sigma-power
end

;; Report time needed to reach node y from x
to-report weight[ x y ]
  let max-speed 1.2 ; maximum speed in the model
  let mean-speed (list max-speed (max-speed * (sigma item y open-visibility) * (item y open-pmove)))
  let delay 0
  carefully [
    set delay ((rep-size / mean-speed) - (rep-size / max-speed))
  ] [ ]
  report ((matrix:get cmaprix x y) / max-speed) + delay
end

;; Report time needed to reach y from agent's position
to-report start-weight[ agent y ]
  let op item y openings ; patches that compose the opening y
  let max-speed 1.2 ; maximum speed in the model
  let mean-speed min (list max-speed (max-speed * (sigma item y open-visibility) * (item y open-pmove)))
  let t-patch nobody
  ask agent [set t-patch patch-here]
  let rnode min-one-of op [distance t-patch] ; nearest opening's patch
  let dist -1
  ask rnode [ set dist distance t-patch] ; distance between agent and nearest opening's patch
  ; delay calculation as above
  let delay 0
  carefully [
    let del-size rep-size
    if dist < del-size [
      set del-size dist
    ]
    set delay ((del-size / mean-speed) - (del-size / max-speed))
  ] [ ]
  report (dist / max-speed) + delay
end

;; Report the size radius defined in equation 13
to-report rep-size
  let double-size (side-size * 2) ; side-size is defined in the user interface
  let b (5 + double-size)
  let h (1 + double-size)

  let diagonal (sqrt (b ^ 2 + h ^ 2))

  report diagonal
end

;; Reconstruct the path calculated with A*
to-report recontruct-path[ came-from cnode ]
  if item cnode came-from > -1 [
    let p recontruct-path came-from item cnode came-from
    report lput cnode p
  ]
  report (list cnode)
end

;; Report number normalized in respect to the range [minimum, maximum]
to-report normalitation[ number minimum maximum ]
  report (number - minimum) / (maximum - minimum)
end

;; Locate the agents in room 0 (the hallway), no overlaps allowed
to set-agents-room-0 [number]
  let part number * 0.1
  ask turtles with [ who < part ] [
    setxy (random 17) + 20 (random 100) + 1
    set room 0
  ]
  while [count patches with [count turtles-on self > 1] > 0] [
    ask patches with [count turtles-on self > 1] [
      ask one-of turtles-on self [
        setxy (random 17) + 20 (random 100) + 1
      ]
    ]
  ]
end

;; Locate the agents in room 1, no overlaps allowed
to set-agents-room-1 [number]
  ifelse not agent-zero [
    ask turtles with [ who > (number * 0.4) - 1 and who < (number * 0.4) * 2]  [
      setxy (random 40) + 39 (random 40) + 62
      set room 1
    ]
  ][
    ask turtles with [ who > (number * 0.45) - 1 and who < (number * 0.8)]  [
      setxy (random 40) + 39 (random 40) + 62
      set room 1
    ]
  ]

  while [count patches with [count turtles-on self > 1] > 0] [
    ask patches with [count turtles-on self > 1] [
      ask one-of turtles-on self [
        setxy (random 40) + 39 (random 40) + 62
      ]
    ]
  ]
end

;; Locate the agents in room 2, no overlaps allowed
to set-agents-room-2 [number]
  ifelse not agent-zero [
    ask turtles with [ who < number * 0.4]  [
      setxy (random 40) + 39 (random 40) + 21
      set room 2
    ]
  ] [
    ask turtles with [ who > (number * 0.1) - 1  and who < (number * 0.45)]  [
      setxy (random 40) + 39 (random 40) + 21
      set room 2
    ]
  ]
  while [count patches with [count turtles-on self > 1] > 0] [
    ask patches with [count turtles-on self > 1] [
      ask one-of turtles-on self [
        setxy (random 40) + 39 (random 40) + 21
      ]
    ]
  ]
end

;; Locate the agents in room 4, no overlaps allowed
to set-agents-room-4 [number]
  let part number * 0.8
  ask turtles with [ who > part - 1 ] [
    setxy (random 18) + 1 (random 32) + 36
    set room 4
  ]
  while [count patches with [count turtles-on self > 1] > 0] [
    ask patches with [count turtles-on self > 1] [
      ask one-of turtles-on self [
        setxy (random 18) + 1 (random 32) + 36
      ]
    ]
  ]
end

;; Set openings' visibility
to set-open-visibility
  let vis-value []

  ; for each opening compute the patch-set to consider
  foreach openings [ opening ->
    let coord [] ; list containing xmin, xmax, ymin, ymax

    ask one-of opening with-min [pxcor] [set coord lput (pxcor - side-size) coord]
    ask one-of opening with-max [pxcor] [set coord lput (pxcor + side-size) coord]
    ask one-of opening with-min [pycor] [set coord lput (pycor - side-size) coord]
    ask one-of opening with-max [pycor] [set coord lput (pycor + side-size) coord]

    let pro-visib 1
    let num 0
    ask building with [ ; ask patches that belong to the opening taken into consideration
      pxcor > item 0 coord and pxcor < item 1 coord
      and
      pycor > item 2 coord and pycor < item 3 coord
    ] [
      set pro-visib (pro-visib * smoke-visibility)
      set num (num + 1)
    ]
    set vis-value lput (pro-visib ^ (1 / num)) vis-value ; compute the geometric mean
  ]
  let n 0
  ; ticks >= 15 is used to avoid overcomputation of A* when the agents are not alerted
  while [n < (length open-visibility) and ticks >= 15] [
    let absol (abs item n open-visibility - item n vis-value)
    if absol > 0 [ ; if the absolute difference is greater than zero then there is a relevant change
      ; the 2 value is temporary, it will change in set-open-pmove that has to be called after this method
      set varya replace-item n varya 2
    ]
    set n n + 1
  ]

  set open-visibility vis-value
end

;; Set openings' move probability
to set-open-pmove

  let pmove-value []

  ; for each opening compute the patch-set to consider
  foreach openings [ opening ->

    let coord [] ; list containing xmin, xmax, ymin, ymax

    ask one-of opening with-min [pxcor] [set coord lput (pxcor - side-size) coord]
    ask one-of opening with-max [pxcor] [set coord lput (pxcor + side-size) coord]
    ask one-of opening with-min [pycor] [set coord lput (pycor - side-size) coord]
    ask one-of opening with-max [pycor] [set coord lput (pycor + side-size) coord]

    let pro-pmove 1
    let num 0

    let bld building with [ ; patches that belong to the opening taken into consideration
      pxcor > item 0 coord and pxcor < item 1 coord
      and
      pycor > item 2 coord and pycor < item 3 coord
    ]
    ask bld [
      set pro-pmove (pro-pmove * (calc-pmove blocks))
      set num (num + 1)
    ]
    set pmove-value lput (pro-pmove ^ (1 / num)) pmove-value ; compute the geometric mean
  ]

  let n 0
  ; ticks >= 15 is used to avoid overcomputation of A* when the agents are not allerted
  while [n < (length open-pmove) and ticks >= 15] [
    let absol (abs item n open-pmove - item n pmove-value)
    ; if the absolute difference is greater than the threshold then there is a relevant change
    ; the threshold is defined in the user interface
    ifelse absol > threshold [
      set varya replace-item n varya 1
    ] [
      ifelse item n varya = 2 [ ; varya = 2 means that there is a significant change in smoke visibility
        set varya replace-item n varya 1 ; so a variation happened
      ][
        set varya replace-item n varya 0
      ]
    ]
    set n n + 1
  ]

  set open-pmove pmove-value

end

;; Report the move probability given the blocks in the patch
to-report calc-pmove [patch-blocks]
  report 1 - ((sum patch-blocks) / (length patch-blocks))
end

;; Setup the plot that shows the exit usage
to setup-plot-exits
  set exit-counter (list 0 0)
end

;; Update the plot that shows the exit usage
to update-plot-exits
  set-current-plot "Exit usage"
  plot-pen-reset
  set-plot-x-range 0 1
  set-plot-pen-mode 1 ; bar mode
  set-plot-pen-color orange
  plot item 0 exit-counter
  set-plot-pen-color blue
  plot item 1 exit-counter
end
@#$#@#$#@
GRAPHICS-WINDOW
258
14
590
435
-1
-1
4.0
1
10
1
1
1
0
0
0
1
0
80
0
102
1
1
1
ticks
30.0

BUTTON
6
15
69
48
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
84
15
149
48
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

PLOT
4
104
255
254
Pedestrian population
Time (ticks)
# Pedestrians
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -2674135 true "" "plot count turtles"

BUTTON
27
58
129
91
NIL
go-nocontrol
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SLIDER
594
14
766
47
side-size
side-size
0
50
5.0
1
1
NIL
HORIZONTAL

SLIDER
594
49
766
82
threshold
threshold
0
1
0.3
0.05
1
NIL
HORIZONTAL

SLIDER
595
87
767
120
n-agents
n-agents
1
1001
401.0
10
1
NIL
HORIZONTAL

SLIDER
593
124
765
157
alert-tick
alert-tick
0
200
120.0
1
1
NIL
HORIZONTAL

SLIDER
594
160
766
193
sigma-power
sigma-power
1
5
4.0
0.1
1
NIL
HORIZONTAL

CHOOSER
615
202
753
247
plot-choice
plot-choice
0 1 2 3
3

CHOOSER
616
255
754
300
select-map
select-map
"0-smoke" "1-smoke" "2-smoke"
1

PLOT
8
284
256
434
Exit usage
NIL
exit usage
0.0
10.0
0.0
10.0
true
false
"setup-plot-exits" "update-plot-exits"
PENS
"pen-0" 1.0 1 -2674135 true "" "update-plot-exits"

SWITCH
628
308
745
341
agent-zero
agent-zero
1
1
-1000

@#$#@#$#@
## WHAT IS IT?

Model simulate the pedestrian evacuation from a building on fire.

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

Setup for setupping and go for going.

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
true
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
